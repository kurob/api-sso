<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Employee;

class BirthdayController extends Controller
{
    public function index()
    {   
        return Employee::getBirthday();
    }
}
