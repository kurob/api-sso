<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Inews;
class InewsController extends Controller
{
    public function index()
    {
        $new = Inews::orderBy('inews_id', 'desc')->paginate(10);
        $new->transform(function ($item, $key) {
            $emp = Inews::select( 'inews_id', 'inews_pic', 'featured_image', 'inews_nm', 'summary')
                ->where('inews_id',$item->inews_id)
                ->first();
            
            $arr = array_add(
                $emp->toArray(),
                'inews_pics',
                'https://sso.krakatausteel.com/upload/inews/'.$emp->inews_pic
            );

            $arr = array_add(
                $arr,
                'featured_images',
                'https://sso.krakatausteel.com/upload/inews/'.$emp->featured_image
            );

            return $arr;
        });
        return $new;

    }
       
}
