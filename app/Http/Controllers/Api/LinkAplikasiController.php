<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LinkAplikasi;

class LinkAplikasiController extends Controller
{
    public function index()
    {
        return LinkAplikasi::aplikasi();
    }
}
