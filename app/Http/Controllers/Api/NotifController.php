<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use App\Models\Notification;
use App\User;

class NotifController extends Controller
{
    public function store(Request $request)
    {
        /* --butuh parameter--
            nik
            title
            body
            module
        ---------------------*/
        $user = User::find($request->nik);
        if ($user) {
            $body = json_encode(
                [
                    'to' => $user->tokengcm,
                    "title" => $request->title,
                    "body" =>  strip_tags($request->body),
                    "data" => $request->all(),
                    "sound" => 'default',
                    "channelId"=> "SSO",                    
                ]
            );
            $notif = new Notification();
            $notif->module = $request->module;
            $notif->for = $request->nik;
            $notif->data = json_encode($request->all()) ;
            $notif->save();

            if ($user->tokengcm) {
                $client = new Client();
                $response = $client->request('POST', 'https://exp.host/--/api/v2/push/send', [
                    'body' => $body,
                    'headers' => ['Content-Type' => 'application/json'],
                ])->getBody();

                $data = json_decode($response);
                return json_encode($data->data);
            }
        }
    }
    public function view($module = null, $nik = null)
    {
        if ($module && $nik) {
            return Notification::where('for', $nik)
                ->where('module', $module)
                ->orderBy('id','DESC')
                ->paginate(20);
        }
        return Notification::paginate();
    }
    public function hide(Request $request)
    {
        $del = Notification::destroy($request->id);
        if($del){
            return ['status'=>'Hapus berhasil'];
        }
        return ['status'=>'Hapus Gagal'];
    }
}
