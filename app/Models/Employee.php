<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employee';
    protected $primaryKey = 'regno';
    public $timestamps = false;
    
    public function scopeBirthday($query)
    {
        return $query->whereMonth('birthdate', date('m') )
                    ->whereDay('birthdate', date('d') );
    }

    public static function getBirthday()
    {
        return $birthday = Employee::birthday()->get()->map(function($data) {
            $x = 'https://portal.krakatausteel.com/eos/storage/pic/'.$data->regno.'.jpg';
            $ke = date('Y') - date('Y', strtotime($data->birthdate));
            return [
                "regno" => $data->regno,
                "nama" => $data->nama,
                "foto" => $x,
                "date" => $data->birthdate,
                "ke" => $ke
            ];
        });
    }

    public static function url_exists($url) {
        $hdrs = @get_headers($url);
    
        // echo @$hdrs[1]."\n";
    
        return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/',$hdrs[0]) : false;
    }
}
