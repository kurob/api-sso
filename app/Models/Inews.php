<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inews extends Model
{
    protected $table = 'inews';
    protected $primaryKey = 'inews_id';
    public $timestamps = false;
}
