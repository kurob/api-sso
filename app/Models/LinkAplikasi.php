<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LinkAplikasi extends Model
{
    protected $table = 'linkaplikasi';
    protected $primaryKey = 'link_id';
    public $timestamps = false;

    public static function aplikasi()
    {
        return LinkAplikasi::where('link_stat', 1)
            ->where('module','<>',null)
            ->get()->map(function($data) {
            $x = 'https://sso.krakatausteel.com/edit/images/icon/'.$data->icon;
            return [
                "link_id" => $data->link_id,
                "link_name" => $data->link_name,
                "link_address" => $data->link_address,
                "link_publish" => $data->link_publish,
                "link_stat" => $data->link_stat,
                "icon" => $x,
                "module" => $data->module,
            ];
        });
    }
}
