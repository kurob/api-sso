<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User;
        $user->name = "mobile-app";
        $user->email = "kurob1993@gmail.com";
        $user->password = Hash::make("19930413");
        $user->api_token = str_random(30);
        $user->save();
    }
}
