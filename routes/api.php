<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware(['auth:api'])->group(function () {
    Route::get('birthday','Api\BirthdayController@index');
    Route::get('inews','Api\InewsController@index');
    Route::get('notif/view/{module?}/{nik?}','Api\NotifController@view');
    Route::post('notif/hide','Api\NotifController@hide');
    Route::get('aplikasi','Api\LinkAplikasiController@index');
});

Route::post('login','Api\LoginController@index');
Route::post('notif','Api\NotifController@store');